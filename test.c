#if 0
	gcc -ggdb3 -Wextra -Wno-missing-field-initializers \
			-o ${0%.*} \
			$0 &&
 	ret=$?
	if test "$1" != +norun && test "$ret" == 0; then
		cgdb -ex r --args ${0%.*} $@
	fi
	exit
#endif
#define _GNU_SOURCE

#include "grow.h"
#include <assert.h>
#include <stdio.h>

#define len(...) ((ssize_t) (sizeof (__VA_ARGS__) / sizeof *(__VA_ARGS__)))
typedef struct this this;
struct this {
	int a;
};

typedef struct strs strs;
struct strs {
	char **d;
	ssize_t count, cap;
};

typedef struct these these;
struct these {
	struct this *d;
	ssize_t count, cap;
};

void test_gr_grow () {
	struct this *these = 0;
	ssize_t count = 0, cap = 0;

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++) {
		GR_GROW (&these, count, &cap, 1);
	}

	// Honestly, what would you do here?

	free (these);
}

void test_gr_add () {
	struct this *these = 0;
	ssize_t count = 0, cap = 0;

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++)
		GR_ADD (&these, &count, &cap, 1, (struct this []) {{.a = i}});

	assert (count == n_checks);
	assert (cap > count);

	for (int i = 0; i < count; i++)
		assert (these[i].a == i);

	free (these);
}

void test_gr_add_1 () {
	struct this *these = 0;
	ssize_t count = 0, cap = 0;

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++)
		GR_ADD_1 (&these, &count, &cap, (struct this) {.a = i});

	assert (count == n_checks);
	assert (cap > count);

	for (int i = 0; i < count; i++)
		assert (these[i].a == i);

	free (these);
}

void test_gr_add_arr () {
	struct this *these = 0;
	ssize_t count = 0, cap = 0;

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i += 2)
		GR_ADD_ARR (&these, &count, &cap, (struct this []) {{.a = i}, {.a = i + 1}});

	assert (count == n_checks);
	assert (cap > count);

	for (int i = 0; i < count; i++)
		assert (these[i].a == i);

	free (these);
}

void test_gr_add_zero () {
	struct this *these = 0;
	ssize_t count = 0, cap = 0;

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++)
		GR_ADD_ZERO (&these, &count, &cap);

	assert (count == n_checks);
	assert (cap > count);

	for (int i = 0; i < count; i++)
		assert (these[i].a == 0);

	free (these);
}

void test_gr_sadd () {
	struct these these = {};

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++)
		GR_SADD (&these, 1, (struct this []) {{.a = i}});

	assert (these.count == n_checks);
	assert (these.cap > these.count);

	for (int i = 0; i < these.count; i++)
		assert (these.d[i].a == i);

	free (these.d);
}

void test_gr_sadd_with_string () {
	struct strs strs = {};

	int n_checks = 10000;
	char *str = "this is the string";
	for (int i = 0; i < n_checks; i++)
		GR_SADD_1 (&strs, str);

	assert (strs.count == n_checks);
	assert (strs.cap > strs.count);

	for (int i = 0; i < strs.count; i++)
		assert (!strcmp (strs.d[i], str));

	free (strs.d);
}

void test_gr_sadd_1 () {
	struct these these = {};

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++)
		GR_SADD_1 (&these, (struct this []) {{.a = i}});

	assert (these.count == n_checks);
	assert (these.cap > these.count);

	for (int i = 0; i < these.count; i++)
		assert (these.d[i].a == i);

	free (these.d);
}

void test_gr_sarr () {
	struct these these = {};

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i += 2)
		GR_SADD_ARR (&these, (struct this []) {{.a = i}, {.a = i + 1}});

	assert (these.count == n_checks);
	assert (these.cap > these.count);

	for (int i = 0; i < these.count; i++)
		assert (these.d[i].a == i);

	free (these.d);
}

void test_gr_sadd_zero () {
	struct these these = {};

	int n_checks = 10000;
	for (int i = 0; i < n_checks; i++)
		GR_SADD_ZERO (&these);

	assert (these.count == n_checks);
	assert (these.cap > these.count);

	for (int i = 0; i < these.count; i++)
		assert (these.d[i].a == 0);

	free (these.d);
}

int main (int, char **) {

#define REPORT($code) \
	do { \
		printf ("grow test: Starting %s ... ", #$code); \
		fflush (stdout); \
		$code; \
		printf ("passed.\n"); \
	} while (0)

	REPORT (test_gr_grow ());
	REPORT (test_gr_add ());
	REPORT (test_gr_add_1 ());
	REPORT (test_gr_add_arr ());
	REPORT (test_gr_add_zero ());

	REPORT (test_gr_sadd ());
	REPORT (test_gr_sadd_with_string ());
	REPORT (test_gr_sadd_1 ());
	REPORT (test_gr_sarr ());
	REPORT (test_gr_sadd_zero ());

	printf ("All tests passed\n");
	exit (0);
}



