# What

Bog-standard dynamic array functions, static inline, header-only. It
doesn't work with a struct; it just works with the bare variables.
There's no insert, delete, etc, because how often do you do that?

The reason there's no struct or insert/delete is in my experience by
far the most common thing you do with a dynamic array is create it.
You create it and after that you don't care about now many items were
allocated. I bet you 90% of dynamic arrays are created just once and
never afterwards appended to.

This is how I use a dynamic array:

```c
struct things *make_the_things (ssize_t *n_things) {
    struct thing *things = nullptr;
    ssize_t cap = 0;
    for (whatever) {
        struct thing thing = {.foo = "this", .bar = "that"};
        ADD (&things, n_things, &cap, 1, thing);
    }
    return things;
}
```

No one cares how many you allocated.

The disadvantage of this approach is you have to track more variables
and so might make a mistake.
