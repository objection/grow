#pragma once

#ifndef GR_GROW_REALLOC
#include <stdlib.h>
#define GR_GROW_REALLOC realloc
#endif
#ifndef GR_GROW_MEMCPY
#include <string.h>
#define GR_GROW_MEMCPY memcpy
#endif
#include <assert.h>

#define __gr_len(...) ((ssize_t) (sizeof (__VA_ARGS__) / sizeof *(__VA_ARGS__)))

#define GR_GROW($p, $count, $cap, $n_to_add) \
	gr_grow ($p, $count, ($cap), sizeof **($p), $n_to_add)
#define GR_GROW_1($p, $count, $cap) \
	GR_GROW ($p, $count, $cap, 1)

#define GR_ADD($p, $count, $cap, $n_objs_to_add, ...) \
	gr_add ((void **) $p, $count, $cap, sizeof **($p), $n_objs_to_add, __VA_ARGS__)
#define GR_ADD_1($p, $count, $cap, ...) \
	GR_ADD ($p, $count, $cap, 1, &(__VA_ARGS__))
#define GR_ADD_ZERO($p, $count, $cap) \
	GR_ADD ($p, $count, $cap, 1, &(void *) {0})

// "_ARR" -- meaning a real array, like arr[10].
#define GR_ADD_ARR($p, $count, $cap, ...) \
	GR_ADD ($p, $count, $cap, __gr_len (__VA_ARGS__), __VA_ARGS__)

#define GR_SGROW($s, $n_to_add) \
	GR_GROW (&($s)->d, ($s)->count, ($s)->cap, $n_to_add)
#define GR_SGROW_1($s) \
	GR_GROW_1 (&($s)->d, ($s)->count, ($s)->cap)

#define GR_SADD($s, $n_objs_to_add, ...) \
	GR_ADD (&($s)->d, &($s)->count, &($s)->cap, $n_objs_to_add, __VA_ARGS__)
#define GR_SADD_1($s, ...) \
	GR_SADD ($s, 1, &(__VA_ARGS__))
#define GR_SADD_ZERO($s) \
	GR_SADD ($s, 1, &(void *) {0})

// "_ARR" -- meaning a real array, like arr[10].
#define GR_SADD_ARR($s, ...) \
	GR_SADD ($s, __gr_len (__VA_ARGS__), __VA_ARGS__)

static inline void gr_grow (void *p, ssize_t count, ssize_t *cap, ssize_t elem_size,
		ssize_t n_to_add) {
	if (count + n_to_add > *cap) {
		*cap += *cap * 2 + (n_to_add * 2);
		*(void **) p = GR_GROW_REALLOC (*(void **) p, elem_size * *cap);
	}
}

static inline void gr_add(void **p, ssize_t *count, ssize_t *cap, ssize_t elem_size,
		ssize_t n_objs, const void *objs) {
	gr_grow (p, *count, cap, elem_size, n_objs);
    memcpy((char *) *p + *count * elem_size, objs, n_objs * elem_size);
    *count += n_objs;
}
